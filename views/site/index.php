<?php

/* @var $this yii\web\View */
$this->title = 'Игра крестики-нолики: сделайте ход';
/** @var $field \app\models\Field */

?>
<div class="site-index">

    <div class="body-content">

        <div class="row">
            <div class="col-lg-4">
            </div>
            <div class="col-lg-4">

                <h2>Начните новую игру</h2>

                <form action="/?r=site/create-game" method="post">
                    <label about="playerX">Имя первого игрока</label> <input type="text" value="" name="playerX"><br />
                    <label about="playerX">Имя второго игрока</label> <input type="text" value="" name="playerO"><br />
                    <button type="submit" class="btn btn-lg btn-success">Начать игру</button>
                    <input type="hidden" name="_csrf" value="<?=Yii::$app->request->getCsrfToken()?>" />
                </form>
            </div>
            <div class="col-lg-4">
            </div>
        </div>
        <div class="row">
            <div class="col-lg-4">
            </div>
            <div class="col-lg-4">

                <h2>Либо продолжите существующую</h2>
                <div>
                    <?php
                    foreach ($games as $game){
                        echo '<a href="/?r=site/play&id='.$game['id'].'">'.$game['name'].'</a><br />';
                    }
                    ?>
                </div>
            </div>
            <div class="col-lg-4">
            </div>
        </div>

    </div>
</div>
