<?php

/* @var $this yii\web\View */
$this->title = 'Игра крестики-нолики: сделайте ход';
/** @var $field \app\models\Field */

?>
<div class="site-index">

    <div class="body-content">

        <div class="row">
            <div class="col-lg-4">
            </div>
            <div class="col-lg-4">

                <h2>Ход игрока <?=$nextTurnName?></h2>

                <form action="/?r=site/play&id=<?=$id ?>" method="post">
                    <?php
                    do {
                        ?>
                        <div class="row">
                            <?php
                            $i = 0;
                            do {
                                ?>
                                <div class="col-lg-3">
                                    <?= $field->getNextSym() ?>
                                </div>
                                <?php
                            } while (!$field->isNewRow());
                            ?>
                        </div>
                        <?php

                    }while(!$field->isFirst());
                    ?>
                    <button type="submit" class="btn btn-lg btn-success">Сделать ход</button>
                    <input type="hidden" name="_csrf" value="<?=Yii::$app->request->getCsrfToken()?>" />
                </form>
            </div>
            <div class="col-lg-4">
            </div>
        </div>

    </div>
</div>
