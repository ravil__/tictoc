<?php

namespace app\controllers;

use app\models\Field;
use app\models\Game;
use app\models\Storage;
use yii\web\Controller;
use yii\web\NotFoundHttpException;


class SiteController extends Controller
{
    /**
     * Ход игры
     * @return string
     * @throws \yii\base\InvalidConfigException|NotFoundHttpException
     */
    public function actionPlay($id)
    {
        /** @var Game $game */
        $game = Storage::load(Game::class, (string)$id);
        if (!$game){
            throw new NotFoundHttpException('Игра не найдена');
        }
        $step = \Yii::$app->request->post('step');
        if ($step !== null){
            $game->makeStep($step);
            Storage::save($game);
        }
        return $this->render('game',
            [
                'field' => $game->field,
                'nextTurnName' => $game->nextTurnName,
                'id' => $game->id
            ]);
    }

    /**
     * Список игр и начало новой
     * @return string
     * @throws \yii\base\InvalidConfigException
     */
    public function actionIndex()
    {
        $gamesData = Storage::list(
            Game::class,
            'name',
            function ($data)
            {
                return "Игра " . $data['playerX'] . " против " . $data['playerO'];
            });
        return $this->render('index', ['games' => $gamesData]);
    }

    public function actionCreateGame()
    {
        $game= new Game();
        // todo добавить проверку
        $game->playerX = \Yii::$app->request->post('playerX');
        // todo добавить проверку
        $game->playerO = \Yii::$app->request->post('playerO');
        Storage::save($game);
        // todo сделать чтобы можно было сразу играть
        $this->redirect('/?r=site/index');
    }
}
