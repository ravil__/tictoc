<?php
/**
 * Created by PhpStorm.
 * User: ravil
 * Date: 21.05.18
 * Time: 0:23
 */

namespace app\models;


use yii\helpers\FileHelper;

class Storage
{
    const FILE_STORAGE_PATH = '/files/';
    const PROP_DELIMITER = '|';
    const STRINGDELIMITER = '%';
    const VALUE_DELIMITER = '=';

    public static function list(string $className, $dataName , $dataFunction, string $unicField = 'id')
    {
        $basket = $className::getStorageId();
        if (!file_exists(self::getFileName($basket)))
        {
            return [];
        }
        $data = self::getAllData($basket, $unicField);
        $result = [];
        foreach ($data as $string){
            $result[] = [$unicField => $string[$unicField], $dataName => $dataFunction($string)];
        }
        return $result;
    }

    /**
     * пересохраняет файл
     * @param Storageble $class
     */
    public static function save (Storageble $class){
        $basket = $class::getStorageId();
        $unicField = $class::getUnicField();
        $modelData = $class->getDataToStorage();
        $unic = $modelData[$unicField];
        if (file_exists(self::getFileName($basket)))
        {
            $data = self::getAllData($basket, $unicField);
        }else{
            $data = [];
        }

        if (empty($unic)){
            $allUnics = array_keys($data);
            $unic = self::getNextUnic($allUnics);
            $modelData[$unicField] = $unic;
        }
        $data[$unic] = $modelData;
        $resultString = array_reduce($data, function ($carry, $item){
            $substr = '';
            foreach ($item as $key => $val){
                $substr = $substr
                    . \app\models\Storage::PROP_DELIMITER
                    . $key
                    . \app\models\Storage::VALUE_DELIMITER
                    . $val;
            }
            $substr = mb_substr($substr, 1);
            return ($carry === null)? $substr: $carry.\app\models\Storage::STRINGDELIMITER.$substr;
        });
        $fileName = self::getFileName($basket);
        file_put_contents($fileName, $resultString);
    }
    /**
     * @param string $className
     * @param string $unic
     * @param string $unicField
     * @return null|Storageble
     */
    public static function load(string $className, string $unic, string $unicField = 'id')
    {
        $basket = $className::getStorageId();
        if (!file_exists(self::getFileName($basket)))
        {
            return null;
        }
        $data = self::getAllData($basket, $unicField);
        if (!array_key_exists($unic, $data))
        {
            return null;
        }
        $data = $data[$unic];
        $class = new $className();
        $class->loadData($data);
        return $class;

    }

    protected static function getNextUnic($keys)
    {

        return (count($keys) > 0) ? max($keys)+1 : 1;
    }

    protected static function getFileName($basket)
    {
        return \Yii::getAlias("@app" . self::FILE_STORAGE_PATH . $basket);
    }

    /**
     * возвращает все записи в формате двумерного массива, ключи для него - значение уникальной колонки
     * todo добавить всяких проверок и упростить
     * @param $basket
     * @return array ['2' => ['id' => 2, 'name' => 'someName']]
     */
    protected static function getAllData($basket, $unicField)
    {
        $string = file_get_contents(self::getFileName($basket));
        if ($string == ''){
            return [];
        }
        $data = explode(self::STRINGDELIMITER, $string);
        $result = [];
        array_map(function ($string) use ($unicField, &$result){
            str_replace("\n", '', $string);
            // делим каждую строку файла на отдельные пары ключ-значение
            $pairs = explode(\app\models\Storage::PROP_DELIMITER, $string);
            $pairArray = [];
            // внутри пары отделяем ключи от значений
            array_map(function ($pairString) use(&$pairArray) {
                $prop = explode(\app\models\Storage::VALUE_DELIMITER, $pairString);
                $pairArray[$prop[0]] = $prop[1];
                return;
            }, $pairs);
            //  в первом измерении нужно проставить правильные ключи
            $key = (string)$pairArray[$unicField];
            $result[$key] = $pairArray;
            return;
        }, $data);
        return $result;
    }

}
