<?php
/**
 * Created by PhpStorm.
 * User: ravil
 * Date: 20.05.18
 * Time: 23:01
 */

namespace app\models;


use yii\base\BaseObject;

class Field extends BaseObject
{
    const X_SYM = 'X';
    const O_SYM = 'O';

    public $data = [];

    protected $currentPosition = 0;

    /**
     * Отдает текущий символ и сдвигает указатель на один
     * @return string
     */
    public function getNextSym()
    {
        $result = $this->data[$this->currentPosition];
        if ($result === false) {
            $result = self::getRadioButton($this->currentPosition);
        }elseif($result == Game::O_STEP){
            $result = self::O_SYM;
        }elseif ($result == Game::X_STEP){
            $result = self::X_SYM;
        }else{
            throw new \Exception('Неизвестный ход '.$result);
        }

        $this->currentPosition++;
        if (count($this->data) <= $this->currentPosition)
        {
            $this->currentPosition = 0;
        }
        return $result;
    }

    /**
     * Указывает находится ли указательна начале поля
     * @return bool
     */
    public function isFirst()
    {
        return $this->currentPosition == 0;
    }

    /**
     * Проверяет не находимся ли мы в новой строке
     * @return bool
     */
    public function isNewRow()
    {
        return ((($this->currentPosition) % $this->dimention) == 0);
    }

    /**
     * формирует radiobutton элемент
     * @param $value
     * @param string $name
     */
    public static function getRadioButton($value , $name = 'step')
    {
        return '<input type="radio" name="'.$name.'" value="'.$value.'">';
    }

    public function getDimention()
    {
        return Game::DIMENTION;
    }
}
