<?php
/**
 * Created by PhpStorm.
 * User: ravil
 * Date: 20.05.18
 * Time: 17:57
 */

namespace app\models;


class Sl
{
    protected static $services = [];

    const ALL_SERVICES = ['ai' => AiSl::class];

    public static function get($name)
    {
       if(array_key_exists($name, self::ALL_SERVICES) && isset(self::$services[$name]))
       {
           return self::$services[$name];
       }else{
           throw new \Exception('Нет такого сервиса');
       }
    }

    public static function set($name , $content)
    {
        if (!array_key_exists($name, self::ALL_SERVICES)){
            throw new \Exception('Такой сервис не предусмотрен');
        }

        $className =  self::ALL_SERVICES[$name];
        if (!$content instanceof $className)
        {
            throw new \Exception('Такой сервис не предусмотрен');
        }
        self::$services[$name] = $content;
    }
}