<?php
/**
 * Created by PhpStorm.
 * User: ravil
 * Date: 20.05.18
 * Time: 17:57
 */

namespace app\models;


class Ai
{
    protected static $selfContainer = null;

    public $level = 'simple';

    protected function __construct()
    {
    }

    public static function getInstance()
    {
        if (! (self::$selfContainer instanceof self)){
            $result = new self();
            self::$selfContainer = $result;
        }
        return self::$selfContainer;
    }
}