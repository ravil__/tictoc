<?php
/**
 * Created by PhpStorm.
 * User: ravil
 * Date: 21.05.18
 * Time: 0:23
 */

namespace app\models;


use yii\base\BaseObject;

/**
 * Class Game
 * @package app\models
 * @property array stepsArray
 */
class Game extends BaseObject implements Storageble
{
    const DIMENTION = 3;
    const X_STEP = 'X';
    const O_STEP = 'O';

    public $id;
    public $steps;
    public $playerX;
    public $playerO;

    public static function getStorageId()
    {
        return 'game';
    }

    public function getUnicField()
    {
        return 'id';
    }

    public function getDataToStorage()
    {
        return [
            'id' => $this->id ,
            'steps' => $this->steps ,
            'playerX' => $this->playerX ,
            'playerO' => $this->playerO ,
        ];
    }

    public function loadData($data){
        $this->id = ($data['id']) ?? null;
        $this->steps = ($data['steps']) ?? null;
        $this->playerX = ($data['playerX']) ?? null;
        $this->playerO = ($data['playerO']) ?? null;
    }

    public function getField()
    {
        $array = array_fill(0, self::DIMENTION * self::DIMENTION, false);
        foreach ($this->stepsArray as $key => $step){
            $sym = ((($key+1)%2) == 0) ? self::O_STEP : self::X_STEP;
            $array[$step] = $sym;
        }
        return new Field(['data' => $array]);
    }

    public function getStepsArray(){
        $result = explode(',', $this->steps);
        // хак с пустым полем
        if ($result[0] === ''){
            unset($result[0]);
        }
        return $result;
    }

    public function getNextTurnName()
    {
        return (count($this->stepsArray) % 2) == 0 ? (string)$this->playerX : (string)$this->playerO;
    }

    /**
     * @param $step
     */
    public function makeStep($step)
    {
        $delimiter = ($this->steps === '') ? '' : ',';
        $this->steps = $this->steps . $delimiter . (string)$step;
    }
}
