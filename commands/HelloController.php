<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\commands;

use app\models\Ai;
use app\models\AiSl;
use app\models\Sl;
use yii\console\Controller;
use yii\console\ExitCode;


class HelloController extends Controller
{

    public function actionIndex()
    {
        $a= AI::getInstance();
        var_dump($a);

        $a->level = 'hard';

        $b = Ai::getInstance();

        var_dump($b);

        return ExitCode::OK;
    }

    public function actionSl()
    {
        Sl::set('ai', (new AiSl()));

        $a = Sl::get('ai');
        var_dump($a);

        $a->level = 'hard';

        $b = Sl::get('ai');

        var_dump($b);

        return ExitCode::OK;
    }
}
